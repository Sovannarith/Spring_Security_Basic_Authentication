package com.sovannarith.info.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/")
public class MainController {


    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    @ResponseBody
    public String hello(){
        return "Hello World";
    }

    @RequestMapping(value = "/auth", method = RequestMethod.GET)
    @ResponseBody
    public String auth(){
        return "Hey Authenticated User!";
    }

    @RequestMapping(value = "/success/logout", method = RequestMethod.GET)
    @ResponseBody
    public String successLogout(){
        return "You are logout successfully!";
    }


}
